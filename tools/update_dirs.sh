#!/bin/sh

readonly cmakelists="CMakeLists.txt"

update_CMakeLists()
{
   if [ ! -f "${cmakelists}" ]; then
      echo "${PWD}/${cmakelists} does not exist."
      exit 10
   fi

   find * -maxdepth 0 -type d -print0 | tr '\0' '\n' | while read dir; do
      grep -q "add_subdirectory(${dir})" "${cmakelists}" ||
         echo "add_subdirectory(${dir})" | tee -a "${cmakelists}"
   done
}

cd "$(dirname "${0}")/.."
for i in scanvideo projects; do
   cd "${i}"
   pwd
   update_CMakeLists
   cd - >/dev/null
done

