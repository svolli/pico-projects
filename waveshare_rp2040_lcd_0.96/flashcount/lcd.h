
#ifndef LCD_H
#define LCD_H LCD_H

#include <gd.h>

#define LCD_WIDTH  160
#define LCD_HEIGHT 80

void lcd_init();
void lcd_set_gdColor( gdImagePtr im );
void lcd_set_gdImage( gdImagePtr im );

#endif
