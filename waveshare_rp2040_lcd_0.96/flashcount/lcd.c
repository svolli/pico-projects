/**
 * Copyright (c) 2023 SvOlli
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <boards/waveshare_rp2040_lcd_0.96.h>

#include <pico/stdlib.h>
#include <hardware/spi.h>
#include <hardware/i2c.h>
#include <hardware/pwm.h>

#include "lcd.h"

#define LCD_RST_PIN  WAVESHARE_RP2040_LCD_RST_PIN
#define LCD_DC_PIN   WAVESHARE_RP2040_LCD_DC_PIN
#define LCD_BL_PIN   13

#define LCD_CS_PIN   WAVESHARE_RP2040_LCD_CS_PIN
#define LCD_CLK_PIN  WAVESHARE_RP2040_LCD_SCLK_PIN
#define LCD_MOSI_PIN WAVESHARE_RP2040_LCD_TX_PIN

#define LCD_SCL_PIN  PICO_DEFAULT_I2C_SCL_PIN
#define LCD_SDA_PIN  PICO_DEFAULT_I2C_SDA_PIN

static uint16_t lcd_gdColors[gdMaxColors];
static uint16_t lcd_linebuffer[LCD_WIDTH];


static inline void lcd_send_command( uint8_t reg )
{
   gpio_put( LCD_DC_PIN, 0 );
	gpio_put( LCD_CS_PIN, 0 );
   spi_write_blocking( spi1, &reg, 1 );
}

static inline void lcd_send_data8( uint8_t data )
{
   gpio_put( LCD_DC_PIN, 1 );
	gpio_put( LCD_CS_PIN, 0 );
   spi_write_blocking( spi1, &data, 1 );
	gpio_put( LCD_CS_PIN, 1 );
}

static inline void lcd_send_command_sequence( uint8_t *data, size_t datasize )
{
   /* first byte command, rest data */
   uint i;

   lcd_send_command( *(data++) );
   for( i = 1; i < datasize; ++i )
   {
      lcd_send_data8( *(data++) );
   }
}


void lcd_set_gdColor( gdImagePtr im )
{
   int i;
   for( i = 0; i < gdMaxColors; ++i )
   {
      uint16_t lcdcolor =
         ((gdImageRed  ( im, i ) & 0xF8) << 10) |
         ((gdImageGreen( im, i ) & 0xFC) << 3) |
         ((gdImageBlue ( im, i ) & 0xF8) >> 3);
      lcd_gdColors[i] = ((lcdcolor << 8) & 0xff00) | (lcdcolor >> 8);
   }
}

void lcd_set_gdImage( gdImagePtr im )
{
   uint8_t seq_tl[] = { 0x2a, 0x00, 0x01, 0x00, 0xa0 }; // Xoffset = 1
   uint8_t seq_br[] = { 0x2b, 0x00, 0x1a, 0x00, 0x69 }; // Yoffset = 26

   uint16_t x, y;
   

   lcd_send_command_sequence( &seq_tl[0], sizeof(seq_tl) );
   lcd_send_command_sequence( &seq_br[0], sizeof(seq_br) );
	lcd_send_command( 0x2C );

   gpio_put( LCD_DC_PIN, 1 );

   for( y = 0; y < LCD_HEIGHT; ++y )
   {
      uint8_t  *in  = im->pixels[y];
      uint16_t *out = &lcd_linebuffer[0];
      for( x = 0; x < LCD_WIDTH; ++x )
      {
         *(out++) = lcd_gdColors[*(in++)];
      }
      spi_write_blocking( spi1, (uint8_t*)&lcd_linebuffer[0], LCD_WIDTH * 2 );
   }
}


void lcd_init()
{
   int slice_num;
   uint8_t seq1[] = { 0xB1, 0x05,0x3A,0x3A };
   uint8_t seq2[] = { 0xB2, 0x05,0x3A,0x3A };
   uint8_t seq3[] = { 0xB3, 0x05,0x3A,0x3A,0x05,0x3A,0x3A };
   uint8_t seq4[] = { 0xB4, 0x03 };
   uint8_t seq5[] = { 0xC0, 0x62,0x02,0x04 };
	uint8_t seq6[] = { 0xC1, 0xC0 };
	uint8_t seq7[] = { 0xC2, 0x0D,0x00 };
	uint8_t seq8[] = { 0xC3, 0x8D,0x6A };
	uint8_t seq9[] = { 0xC4, 0x8D,0xEE };
	uint8_t seq10[] = { 0xC5, 0x0E };  /*VCOM*/
	uint8_t seq11[] = { 0xE0, 0x10,0x0E,0x02,0x03,0x0E,0x07,0x02,0x07,0x0A,0x12,0x27,0x37,0x00,0x0D,0x0E,0x10 };
	uint8_t seq12[] = { 0xE1, 0x10,0x0E,0x03,0x03,0x0F,0x06,0x02,0x08,0x0A,0x13,0x26,0x36,0x00,0x0D,0x0E,0x10 };
	uint8_t seq13[] = { 0x3A, 0x05 };
	uint8_t seq14[] = { 0x36, 0xA8 };

   // SPI Config
   spi_init( spi1, 10000 * 1000 );
   gpio_set_function( LCD_CLK_PIN, GPIO_FUNC_SPI );
   gpio_set_function( LCD_MOSI_PIN, GPIO_FUNC_SPI );

   // GPIO Config
   gpio_init( LCD_RST_PIN );
   gpio_set_dir( LCD_RST_PIN, GPIO_OUT );
   gpio_init( LCD_DC_PIN );
   gpio_set_dir( LCD_DC_PIN, GPIO_OUT );
   gpio_init( LCD_CS_PIN );
   gpio_set_dir( LCD_CS_PIN, GPIO_OUT );
   gpio_init( LCD_BL_PIN );
   gpio_set_dir( LCD_BL_PIN, GPIO_OUT );

   gpio_put( LCD_CS_PIN, 1 );
   gpio_put( LCD_DC_PIN, 0 );
   gpio_put( LCD_BL_PIN, 1 );
   
   // PWM Config
   gpio_set_function( LCD_BL_PIN, GPIO_FUNC_PWM );
   slice_num = pwm_gpio_to_slice_num( LCD_BL_PIN );
   pwm_set_wrap( slice_num, 100 );
   pwm_set_chan_level( slice_num, PWM_CHAN_B, 1 );
   pwm_set_clkdiv( slice_num,50 );
   pwm_set_enabled( slice_num, true );

   //I2C Config
   i2c_init( i2c1, 300*1000 );
   gpio_set_function( LCD_SDA_PIN, GPIO_FUNC_I2C );
   gpio_set_function( LCD_SCL_PIN, GPIO_FUNC_I2C );
   gpio_pull_up( LCD_SDA_PIN );
   gpio_pull_up( LCD_SCL_PIN );

   /* set pwm */
   pwm_set_chan_level( slice_num, PWM_CHAN_B, 90 );
   
   /* reset */
	gpio_put( LCD_RST_PIN, 1 );
   sleep_ms( 200 );
   gpio_put( LCD_RST_PIN, 0 );
   sleep_ms( 200 );
   gpio_put( LCD_RST_PIN, 1 );
   sleep_ms( 200 );

   /* register init */
   lcd_send_command( 0x11 );//Sleep exit 
	sleep_ms( 120 );
	lcd_send_command( 0x21 );
	lcd_send_command( 0x21 );
   
   lcd_send_command_sequence( &seq1[0], sizeof(seq1) );
   lcd_send_command_sequence( &seq2[0], sizeof(seq2) );
   lcd_send_command_sequence( &seq3[0], sizeof(seq3) );
   lcd_send_command_sequence( &seq4[0], sizeof(seq4) );
   lcd_send_command_sequence( &seq5[0], sizeof(seq5) );
   lcd_send_command_sequence( &seq6[0], sizeof(seq6) );
   lcd_send_command_sequence( &seq7[0], sizeof(seq7) );
   lcd_send_command_sequence( &seq8[0], sizeof(seq8) );
   lcd_send_command_sequence( &seq9[0], sizeof(seq9) );
   lcd_send_command_sequence( &seq10[0], sizeof(seq10) );
   lcd_send_command_sequence( &seq11[0], sizeof(seq11) );
   lcd_send_command_sequence( &seq12[0], sizeof(seq12) );
   lcd_send_command_sequence( &seq13[0], sizeof(seq13) );
   lcd_send_command_sequence( &seq14[0], sizeof(seq14) );

	lcd_send_command( 0x29 );
}
