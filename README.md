Pico-Projects
=============

This is a list of small projects either not big enough for an own repository
or which are just proof-of-concepts, tests and templates.

Besides the always required `pico-sdk` this project will also require the
`pico-extras` repository to do some kinky stuff not available in the current
sdk so far, like VGA.

This repository uses submodules.

Since builds might be for different boards (Pico, Pico W, etc.) those are
implemented using different directories. Code used commonly by boards are in
the `common` directory.


Useful Things
-------------
Each platform contains a `flashcount` project, that detects the size of the
flash and uses the LED to indicate detected size one flash per MB.

`common/libgd` contains a wrapper including the [libgd](https://libgd.org/)
submodule. This is an easy way to create graphics for displays connected
to the pico.

