
set(COMMON_PATH "${CMAKE_CURRENT_LIST_DIR}" CACHE PATH "Path to shared code" FORCE)

function(add_common_directory shortname)
   add_subdirectory("${COMMON_PATH}/${shortname}" "${shortname}")
endfunction()

