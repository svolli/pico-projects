
#ifndef LIBOPC_GDFONT4X8_H
#define LIBOPC_GDFONT4X8_H LIBOPC_GDFONT4X8_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "gd.h"

extern gdFontPtr gdFont4x8;
BGD_DECLARE(gdFontPtr) gdFontGet4x8(void);

#ifdef __cplusplus
}
#endif

#endif
