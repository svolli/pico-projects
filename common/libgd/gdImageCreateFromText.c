
#include "gd_db.h"

#include <string.h>
#include <stdlib.h>

gdImagePtr gdImageCreateFromText( gdFontPtr font, const char *text, int bgcol, int txtcol, int padleft, int padright )
{
   gdImagePtr im;
   unsigned char *t = (unsigned char*)strdup( text );

   im = gdImageCreateTrueColor( (strlen( text ) * font->w) + padleft + padright, font->h );
   gdImageFilledRectangle( im, 0, 0, im->sx-1, im->sy-1, bgcol );
   gdImageString( im, font, padleft, 0, t, txtcol );
   free( t );

   return im;
}
