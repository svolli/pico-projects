
#ifndef GD_DB_H
#define GD_DB_H GD_DB_H

#include <gd.h>

#include <gdfont4x8.h>
#include <gdfont6x8.h>
#include <gdfont8x8.h>

void gdImageCopyAdd( gdImagePtr dst, gdImagePtr src, int dstX, int dstY,
                     int srcX, int srcY, int w, int h );
void gdImageCopyMultiply( gdImagePtr dst, gdImagePtr src, int dstX, int dstY,
                          int srcX, int srcY, int w, int h );
gdImagePtr gdImageCreateFromText( gdFontPtr font, const char *text, int bgcol, int txtcol, int padleft, int padright );
void gdImageFilledEllipseFast( gdImagePtr im, int mx, int my, int w, int h, int c );

#endif

