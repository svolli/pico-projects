
#ifndef LIBOPC_GDFONT8X8_H
#define LIBOPC_GDFONT8X8_H LIBOPC_GDFONT8X8_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "gd.h"

extern gdFontPtr gdFont8x8;
BGD_DECLARE(gdFontPtr) gdFontGet8x8(void);

#ifdef __cplusplus
}
#endif

#endif
