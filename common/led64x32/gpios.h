
#ifndef GPIOS_H_
#define GPIOS_H_ GPIOS_H_

// UART: 0 & 1
#define GPIO_R1           (2)
#define GPIO_G1           (3)
#define GPIO_B1           (4)
#define GPIO_R2           (5)
#define GPIO_DS3231_SDA   (6)
#define GPIO_DS3231_SCL   (7)
#define GPIO_G2           (8)
#define GPIO_B2           (9)
#define GPIO_A0           (10)
#define GPIO_CLK          (11)
#define GPIO_STB          (12)
#define GPIO_OE           (13)
// unused: 14
#define GPIO_KEY0_PIN     (15)
#define GPIO_A1           (16)
// unused: 17
#define GPIO_A2           (18)
#define GPIO_KEY1_PIN     (19)
#define GPIO_A3           (20)
#define GPIO_KEY2_PIN     (21)
#define GPIO_A4           (22)

#define GPIO_ONBOARD_LED  (25) /* NOT on Pico W */
#define GPIO_LIGHT_SENSOR (26)
#define GPIO_BUZZER       (27)
#define GPIO_IR           (28)

#define GPIO_DS3231_PORT  (i2c1)

#define DS3231_ADDRESS    (0x68)

#endif
