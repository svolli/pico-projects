
#include <stdbool.h>

#include <pico.h>
#include <pico/stdlib.h>
#include <pico/time.h>
#include <hardware/adc.h>
#include <hardware/i2c.h>
#include <hardware/structs/i2c.h>
#include <hardware/regs/dreq.h>

#include "led64x32.h"

#include "gpios.h"

#define DS3231_WRITE                (0)
#define DS3231_READ                 (1)

#define DS3231_CTRL_OSCILLATOR      (0x80)
#define DS3231_CTRL_SQUAREWAVE_BB   (0x40)
#define DS3231_CTRL_TEMPCONV        (0x20)

#define DS3231_ADDR_TIME            (0x00)
#define DS3231_ADDR_CONTROL         (0x0e)
#define DS3231_ADDR_STATUS          (0x0f)
#define DS3231_ADDR_AGING           (0x10)
#define DS3231_ADDR_TEMP            (0x11)

#define DS3231_12HOUR_FLAG          (0x40)
#define DS3231_12HOUR_MASK          (0x1f)
#define DS3231_PM_FLAG              (0x20)
#define DS3231_MONTH_MASK           (0x1f)

#define DS3231_ADDR_TIME_SEC        (0x00)
#define DS3231_ADDR_TIME_MIN        (0x01)
#define DS3231_ADDR_TIME_HOUR       (0x02)
#define DS3231_ADDR_TIME_WDAY       (0x03)
#define DS3231_ADDR_TIME_MDAY       (0x04)
#define DS3231_ADDR_TIME_MON        (0x05)
#define DS3231_ADDR_TIME_YEAR       (0x06)


static inline uint8_t bcd2bin( uint8_t bcd )
{
    return ((bcd >> 4) * 10) + (bcd & 0x0f);
}

static inline uint8_t bin2bcd( uint8_t bin )
{
    return ((bin / 10) << 4) + (bin % 10);
}


void ds3231_init()
{
   uint8_t buf[2];

   i2c_init( GPIO_DS3231_PORT, 400000 );
   gpio_set_function( GPIO_DS3231_SDA, GPIO_FUNC_I2C );
   gpio_set_function( GPIO_DS3231_SCL, GPIO_FUNC_I2C );
   gpio_pull_up( GPIO_DS3231_SDA );
   gpio_pull_up( GPIO_DS3231_SCL );

   buf[0] = DS3231_ADDR_CONTROL;
   buf[1] = DS3231_CTRL_TEMPCONV;
   i2c_write_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, buf, sizeof(buf), false );

   buf[0] = DS3231_ADDR_STATUS;
   buf[1] = DS3231_ADDR_TIME;
   i2c_write_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, buf, sizeof(buf), false );
}

void ds3231_get_time( DS3231_tm *tm )
{
   uint8_t data[7];
   uint8_t val = DS3231_ADDR_TIME;
   i2c_write_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, &val, sizeof(val), true );
   i2c_read_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, data, sizeof(data), false );
   tm->tm_sec  = bcd2bin( data[0] );
   tm->tm_min  = bcd2bin( data[1] );
   tm->tm_hour = bcd2bin( data[2] ); /* 24H expected, 12H: DS3231_12HOUR_FLAG set */
   tm->tm_wday = bcd2bin( data[3] );
   tm->tm_mday = bcd2bin( data[4] );
   tm->tm_mon  = bcd2bin( data[5] & DS3231_MONTH_MASK );
   tm->tm_year = bcd2bin( data[6] ) + 2000;
}

void ds3231_set_time( DS3231_tm *tm )
{
    uint8_t data[8];
    data[0] = DS3231_ADDR_TIME;
    /* time/date data */
    data[1] = bin2bcd( tm->tm_sec );
    data[2] = bin2bcd( tm->tm_min );
    data[3] = bin2bcd( tm->tm_hour );
    /* The week data must be in the range 1 to 7, and to keep the start on the
     * same day as for tm_wday have it start at 1 on Sunday. */
    data[4] = bin2bcd( tm->tm_wday );
    data[5] = bin2bcd( tm->tm_mday );
    data[6] = bin2bcd( tm->tm_mon );
    data[7] = bin2bcd( tm->tm_year - 2000 );
    i2c_write_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, data, sizeof(data), false );
}

uint8_t ds3231_get_temp_integer()
{
    const uint8_t start_temperature[2] = { DS3231_ADDR_CONTROL, DS3231_CTRL_TEMPCONV };
    const uint8_t val = DS3231_ADDR_TEMP;
    uint8_t data;

    i2c_write_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, start_temperature, sizeof(start_temperature), false );
    i2c_write_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, &val, sizeof(val), true );
    i2c_read_blocking( GPIO_DS3231_PORT, DS3231_ADDRESS, &data, sizeof(data), false );

    return data;
}

