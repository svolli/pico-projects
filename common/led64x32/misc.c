
#include "gpios.h"

#include <stdbool.h>

#include <pico.h>
#include <pico/stdlib.h>
#include <pico/time.h>
#include <hardware/adc.h>
#include <hardware/i2c.h>
#include <hardware/structs/i2c.h>
#include <hardware/regs/dreq.h>

#include "led64x32.h"



void buzzer_init()
{
   gpio_init( GPIO_BUZZER );
   gpio_set_dir( GPIO_BUZZER, GPIO_OUT );
   gpio_put( GPIO_BUZZER, 0 );
}


void buzzer_set( int val )
{
   gpio_put( GPIO_BUZZER, val );
}


void lightsensor_init()
{
   adc_init();
   adc_gpio_init( GPIO_LIGHT_SENSOR );
   adc_select_input( 0 );
}


uint16_t lightsensor_get()
{
   return adc_read();
}


static inline void setup_key( uint pin )
{
   gpio_init( pin );
   gpio_set_dir( pin, GPIO_IN );
   gpio_pull_up( pin );
}

void keys_init()
{
   setup_key( GPIO_KEY0_PIN );
   setup_key( GPIO_KEY1_PIN );
   setup_key( GPIO_KEY2_PIN );
}

