
#ifndef TREMOR_IN_ROM_H
#define TREMOR_IN_ROM_H TREMOR_IN_ROM_H

#include <tremor/ivorbisfile.h>

int ov_load_data( const char *data, size_t size, OggVorbis_File *vf );

#endif
