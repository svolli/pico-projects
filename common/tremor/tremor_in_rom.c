
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "tremor_in_rom.h"


typedef struct ogginrom_s
{
   char     *data;
   size_t   size;
   off_t    position;
   int      malloced;
} ogginrom_t;


static size_t oir_read( void *ptr, size_t size, size_t nmemb, void *datasource )
{
   ogginrom_t *oir = (ogginrom_t*)datasource;
   size_t copysize = oir->size - oir->position;
   if( (size * nmemb) < copysize )
   {
      copysize = (size * nmemb);
   }

   memcpy( ptr, oir->data + oir->position, copysize );
   oir->position += copysize;
   return (copysize + size - 1) / size;
}


static int oir_seek( void *datasource, ogg_int64_t offset, int whence )
{
   ogginrom_t *oir = (ogginrom_t*)datasource;
   off_t base;

   switch( whence )
   {
   case SEEK_SET:
      base = 0;
      break;
   case SEEK_CUR:
      base = oir->position;
      break;
   case SEEK_END:
      base = oir->size;
      break;
   default:
      printf( "illegal whence: %d\n", whence );
      exit( 1 );
      break;
   }

   oir->position = (off_t)offset + base;

   return (int)oir->position;
}


static int oir_close( void *datasource )
{
   ogginrom_t *oir = (ogginrom_t*)datasource;

   if( (oir->malloced) && (oir->data) )
   {
      free( oir->data );
   }
   free( oir );

   return 0;
}


static long oir_tell( void *datasource )
{
   ogginrom_t *oir = (ogginrom_t*)datasource;

   return (long) oir->position;
}


static ogginrom_t *oir_open( const char *filename )
{
   ogginrom_t *oir = 0;

   int fd;
   ssize_t dataread;
   struct stat st;

   fd = open( filename, O_RDONLY );
   if( fd < 0 )
   {
      perror( filename );
      exit( 1 );
   }
   if( fstat( fd, &st ) < 0 )
   {
      perror( filename );
      exit( 1 );
   }

   oir = malloc( sizeof(*oir) );
   if( !oir )
   {
      perror( "malloc" );
      exit( EXIT_FAILURE );
   }
   oir->data = malloc( st.st_size );
   oir->size = st.st_size;
   oir->position = 0;
   oir->malloced = 1;
   if( !(oir->data) )
   {
      perror( "malloc" );
      exit( EXIT_FAILURE );
   }
   for( oir->position = 0; oir->position < st.st_size; oir->position += dataread )
   {
      dataread = read( fd, oir->data + oir->position, st.st_size - oir->position );
      if( dataread < 0 )
      {
         perror( filename );
         exit( 1 );
      }
      oir->position += dataread;
   }
   close( fd );
   oir->position = 0;

   return oir;
}


int ov_load_data( const char *data, size_t size, OggVorbis_File *vf )
{
   ov_callbacks ovc = { oir_read, oir_seek, oir_close, oir_tell };

   ogginrom_t *oir = malloc( sizeof(*oir) );
   if( !oir )
   {
      perror( "malloc" );
      exit( EXIT_FAILURE );
   }
   oir->data = (char*)data;
   oir->size = size;
   oir->position = 0;
   oir->malloced = 0;

   return ov_open_callbacks( oir, vf, 0, 0, ovc );
}
