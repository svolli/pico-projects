
#include <led64x32.h>
#include <pico.h>
#include <pico/stdlib.h>
#include <hardware/uart.h>

#include <stdio.h>

#include "../libgd/gdfont8x8.h"

const uint64_t man1[] = {
   0x0000011111100000,
   0x0000100000010000,
   0x0000101010010000,
   0x0011100000011100,
   0x0100011111100010,
   0x1000000000000001,
   0x1001000000010001,
   0x1011000000010011,
   0x1001000000010001,
   0x1111111111111111,
   0x0100100000001001,
   0x0011100010000111,
   0x0000100010001100,
   0x0001110010000100,
   0x0010000100001100,
   0x0011111111111100
};

gdImagePtr testpattern( gdImagePtr im )
{
   int colors[] = { 7, 6, 3, 2, 5, 4, 1, 0 };
   int bar;
   if( !im )
   {
      im = led_gdImageCreate();
   }

   for( bar = 0; bar < 8; ++bar )
   {
      gdImageFilledRectangle( im, bar*8, 0, bar*8+7, 31, colors[bar] );
   }
   gdImageString( im, gdFontGet8x8(), 12, 12, "XAYAX", 0 );

   return im;
}

int main()
{
   int x, y;
   gdImagePtr im1 = led_gdImageCreate();
   gdImagePtr im2 = led_gdImageCreate();
   gdImagePtr bars = testpattern( 0 );
   gdImagePtr iman1 = led_gdImageCreate();
   gdImagePtr iman2 = led_gdImageCreate();
   DS3231_tm tm;

   stdio_init_all();
   uart_set_translate_crlf(uart0, true);
   //sleep_ms(2000);

   printf( "led image test\n" );

   led_init();
   buzzer_init();
   ds3231_init();
#if 0
   im = led_gdImageCreate();

   for( y = 0; y < gdImageSY(im); ++y )
   {
      for( x = 0; x < gdImageSX(im); ++x )
      {
         gdImageSetPixel( im, x, y, (x+y) & 0xf );
      }
   }
   gdImageLine( im, 0, 0, gdImageSX(im), gdImageSY(im), 6 );

#endif

   led_set_gdImage(bars);
   sleep_ms( 1500 );

   gdImageFilledRectangle( iman1, 0,  0, 63, 23, 1 );
   gdImageFilledRectangle( iman1, 0, 23, 63, 31, 2 );
   gdImageFilledRectangle( iman2, 0,  0, 63, 23, 1 );
   gdImageFilledRectangle( iman2, 0, 23, 63, 31, 2 );

   for( int y = 0 ; y < 16; ++y )
   {
      uint64_t b = 0x1000000000000000;
      for( x = 0; x < 16; ++x, b >>= 4 )
      {
         if( b & man1[y] )
         {
            gdImageSetPixel( iman1, x+24, y+7, 0 );
            gdImageSetPixel( iman2, x+24, y+6, 0 );
         }
      }
   }

   ds3231_get_time( &tm );
   y = tm.tm_sec;
   while( y == tm.tm_sec )
   {
      ds3231_get_time( &tm );
   }

   for( int i = 0;; ++i )
   {
      static char buffer[10];
      gdImagePtr im;

      sleep_ms( 100 );

      ds3231_get_time( &tm );
      sprintf( buffer, "%02d:%02d:%02d", tm.tm_hour, tm.tm_min, tm.tm_sec );

      im = (i & 1) ? im1 : im2;

      if( i < 6 )
      {
         gdImageCopy( im, iman1, 0, 0, 0, 0, im->sx, im->sy );
      }
      else
      {
         gdImageCopy( im, iman2, 0, 0, 0, 0, im->sx, im->sy );
      }
      //gdImageString( im, gdFont8x8, 0, 24, buffer, (tm.tm_sec >> 2) & 7 );
      gdImageString( im, gdFont8x8, 0, 24, buffer, 0 );
      led_set_gdImage( im );

      if( i >= 8 )
      {
         i = 0;
         led_set_brightness( lightsensor_get() < 3600 ? 5 : 1 );
      }
   }
}
