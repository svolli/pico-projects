/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <pico/stdlib.h>
#include <pico/binary_info.h>
#include <stdio.h>
#include <fake_eeprom.h>
#include <gd.h>
#include <gdfont8x8.h>

#include <hardware/spi.h>

#include "lcd.h"
#include "ws_lcd.h"

const uint64_t man1[] = {
   0x0000011111100000,
   0x0000100000010000,
   0x0000101010010000,
   0x0011100000011100,
   0x0100011111100010,
   0x1000000000000001,
   0x1001000000010001,
   0x1011000000010011,
   0x1001000000010001,
   0x1111111111111111,
   0x0100100000001001,
   0x0011100010000111,
   0x0000100010001100,
   0x0001110010000100,
   0x0010000100001100,
   0x0011111111111100
};


gdImagePtr create_image( uint16_t width, uint16_t height )
{
   gdImagePtr im = gdImageCreate( width, height );

   int color0 = gdImageColorAllocate( im,   0,   0,   0 );
   int color1 = gdImageColorAllocate( im,   0,   0, 255 );
   int color2 = gdImageColorAllocate( im,   0, 255,   0 );

   gdImageFilledRectangle( im, 0,  0, (width - 1), height - 1, color1 );
   gdImageFilledRectangle( im, 0, (height / 2), (width - 1), height - 1, color2 );

   for( int y = 0 ; y < 16; ++y )
   {
      uint64_t b = 0x1000000000000000;
      for( int x = 0; x < 16; ++x, b >>= 4 )
      {
         if( b & man1[y] )
         {
            gdImageSetPixel( im, (im->sx - 16)/2 + x, (height / 2) - 16 + y, color0 );
         }
      }
   }

   return im;
}


int main()
{
   const char spinner[] = "/-\\|";
   int s = 0;
   char buffer[21] = { 0 };
   gdImagePtr im = create_image( LCD_WIDTH, LCD_HEIGHT );

   stdio_init_all();
   uart_set_translate_crlf( uart0, true );

   uint32_t flash_size = flash_size_detect() / (1<<20);
   sleep_ms( 2000 );
   printf( "start\n" );

#if 1
   System_Init();
   LCD_SCAN_DIR  lcd_scan_dir = SCAN_DIR_DFT;
   LCD_Init(lcd_scan_dir,500);
#else  
   lcd_init();
#endif
   LCD_Clear(0xFFFF);
   LCD_Clear(0x0000);

   snprintf( buffer, sizeof(buffer)-1, "FLASH SIZE: %2d MB", flash_size );
printf( "%s\n",buffer );
   gdImageString( im, gdFontGet8x8(), 8, LCD_HEIGHT - 16, buffer, 0 );
   lcd_set_gdColor( im );
   lcd_set_gdImage( im );

   for(;;)
   {
      sleep_ms(1000);
      printf( "%c\b", spinner[s++] );
      s &= 0x03;
   }
   return 0;
}
