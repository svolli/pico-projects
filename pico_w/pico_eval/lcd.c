/**
 * Copyright (c) 2023 SvOlli
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <boards/waveshare_rp2040_lcd_0.96.h>

#include <pico/stdlib.h>
#include <hardware/spi.h>
#include <hardware/i2c.h>
#include <hardware/pwm.h>

#include "lcd.h"


static uint16_t lcd_gdColors[gdMaxColors];
static uint16_t lcd_linebuffer[LCD_WIDTH];




static inline void lcd_send_command( uint8_t reg )
{
   gpio_put( LCD_DC_PIN, 0 );
	gpio_put( LCD_CS_PIN, 0 );
   spi_write_blocking( spi1, &reg, 1 );
	gpio_put( LCD_CS_PIN, 1 );
}

static inline void lcd_send_data8( uint8_t data )
{
   uint16_t be = ((uint16_t)data << 8);
   gpio_put( LCD_DC_PIN, 1 );
	gpio_put( LCD_CS_PIN, 0 );
   spi_write_blocking( spi1, &data, 2 );
	gpio_put( LCD_CS_PIN, 1 );
}

static inline void lcd_send_command_sequence( uint8_t *data, size_t datasize )
{
   /* first byte command, rest data */
   uint i;

   lcd_send_command( *(data++) );
   for( i = 1; i < datasize; ++i )
   {
      lcd_send_data8( *(data++) );
   }
}


void lcd_set_gdColor( gdImagePtr im )
{
   int i;
   for( i = 0; i < gdMaxColors; ++i )
   {
#if 1
      lcd_gdColors[i] =
         ((gdImageRed  ( im, i ) & 0xF8) << 10) |
         ((gdImageGreen( im, i ) & 0xFC) << 3) |
         ((gdImageBlue ( im, i ) & 0xF8) >> 3);
#else
      uint16_t lcdcolor =
         ((gdImageRed  ( im, i ) & 0xF8) << 10) |
         ((gdImageGreen( im, i ) & 0xFC) << 3) |
         ((gdImageBlue ( im, i ) & 0xF8) >> 3);
      lcd_gdColors[i] = ((lcdcolor << 8) & 0xff00) | (lcdcolor >> 8);
#endif
   }
}

void lcd_set_gdImage( gdImagePtr im )
{
   uint16_t x, y;

#if 1
   //LCD_SetWindow( 0, 0, LCD_WIDTH, LCD_HEIGHT );
#else
   uint8_t seq_x[] = { 0x2a, 0x00, 0x00, (LCD_WIDTH-1)  >> 8, (LCD_WIDTH-1)  & 0xFF };
   uint8_t seq_y[] = { 0x2b, 0x00, 0x00, (LCD_HEIGHT-1) >> 8, (LCD_HEIGHT-1) & 0xFF };

   lcd_send_command_sequence( &seq_x[0], sizeof(seq_x) );
   lcd_send_command_sequence( &seq_y[0], sizeof(seq_y) );
	lcd_send_command( 0x2c );
#endif

   gpio_put( LCD_DC_PIN, 1 );

   for( y = 0; y < LCD_HEIGHT; ++y )
   {
      uint8_t  *in  = im->pixels[y];
#if 1
      for( x = 0; x < LCD_WIDTH; ++x )
      {
         LCD_SetPointlColor( x, y, lcd_gdColors[*(in++)]);
      }
#else
      uint16_t *out = &lcd_linebuffer[0];
      for( x = 0; x < LCD_WIDTH; ++x )
      {
         *(out++) = lcd_gdColors[*(in++)];
      }
      spi_write_blocking( spi1, (uint8_t*)&lcd_linebuffer[0], LCD_WIDTH * 2 );
#endif
   }
}


void lcd_init()
{
   uint slice_num;

   // SPI Config
   spi_init( spi1, 10000 * 1000 );
   gpio_set_function( LCD_CLK_PIN, GPIO_FUNC_SPI );
   gpio_set_function( LCD_MOSI_PIN, GPIO_FUNC_SPI );

   // GPIO Config
   gpio_init( LCD_RST_PIN );
   gpio_init( LCD_DC_PIN );
   gpio_init( LCD_CS_PIN );
   gpio_init( TP_CS_PIN );
   gpio_init( TP_IRQ_PIN );
   gpio_init( SD_CS_PIN );

   gpio_set_dir( LCD_RST_PIN, GPIO_OUT );
   gpio_set_dir( LCD_DC_PIN, GPIO_OUT );
   gpio_set_dir( LCD_CS_PIN, GPIO_OUT );
   gpio_set_dir( TP_CS_PIN, GPIO_OUT );
   gpio_set_dir( TP_IRQ_PIN, GPIO_IN );
   gpio_set_dir( SD_CS_PIN, GPIO_OUT );

   gpio_put(TP_CS_PIN, 1);
   gpio_put(LCD_CS_PIN, 1);
	
   gpio_put(SD_CS_PIN, 1);

	gpio_set_function( LCD_BKL_PIN, GPIO_FUNC_PWM );

	spi_init(SPI_PORT,5000000);
	gpio_set_function( LCD_CLK_PIN, GPIO_FUNC_SPI );
	gpio_set_function( LCD_MOSI_PIN, GPIO_FUNC_SPI );
	gpio_set_function( LCD_MISO_PIN, GPIO_FUNC_SPI );

	slice_num = pwm_gpio_to_slice_num( LCD_BKL_PIN );
	pwm_set_wrap( slice_num, 10000 );
	pwm_set_chan_level( slice_num, PWM_CHAN_B, 10000 );
	pwm_set_enabled( slice_num, true );

   /* reset */
   gpio_put( LCD_RST_PIN, 1 );
   sleep_ms( 500 );
   gpio_put( LCD_RST_PIN, 0 );
   sleep_ms( 500 );
   gpio_put( LCD_RST_PIN, 1 );
   sleep_ms( 500 );

   lcd_send_command(0x21);
   lcd_send_command(0xC2);	//Normal mode, increase can change the display quality, while increasing power consumption
   lcd_send_data8(0x33);
   
   lcd_send_command(0XC5);
   lcd_send_data8(0x00);
   lcd_send_data8(0x1e);//VCM_REG[7:0]. <=0X80.
   lcd_send_data8(0x80);
   
   lcd_send_command(0xB1);//Sets the frame frequency of full color normal mode
   lcd_send_data8(0xB0);//0XB0 =70HZ, <=0XB0.0xA0=62HZ
   
   lcd_send_command(0x36);
   lcd_send_data8(0x28); //2 DOT FRAME MODE,F<=70HZ.
   
   lcd_send_command(0XE0);
   lcd_send_data8(0x0);
   lcd_send_data8(0x13);
   lcd_send_data8(0x18);
   lcd_send_data8(0x04);
   lcd_send_data8(0x0F);
   lcd_send_data8(0x06);
   lcd_send_data8(0x3a);
   lcd_send_data8(0x56);
   lcd_send_data8(0x4d);
   lcd_send_data8(0x03);
   lcd_send_data8(0x0a);
   lcd_send_data8(0x06);
   lcd_send_data8(0x30);
   lcd_send_data8(0x3e);
   lcd_send_data8(0x0f);		
   
   lcd_send_command(0XE1);
   lcd_send_data8(0x0);
   lcd_send_data8(0x13);
   lcd_send_data8(0x18);
   lcd_send_data8(0x01);
   lcd_send_data8(0x11);
   lcd_send_data8(0x06);
   lcd_send_data8(0x38);
   lcd_send_data8(0x34);
   lcd_send_data8(0x4d);
   lcd_send_data8(0x06);
   lcd_send_data8(0x0d);
   lcd_send_data8(0x0b);
   lcd_send_data8(0x31);
   lcd_send_data8(0x37);
   lcd_send_data8(0x0f);
   
   lcd_send_command(0X3A);	//Set Interface Pixel Format
   lcd_send_data8(0x55);
   
   lcd_send_command(0x11);//sleep out
   sleep_ms(120);
   lcd_send_command(0x29);//Turn on the LCD display
}
